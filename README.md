# edsolabs-video-grid

> video grid components for camera sources rendering, with camera zone layouting supported.

[![NPM](https://img.shields.io/npm/v/edsolabs-video-grid.svg)](https://www.npmjs.com/package/edsolabs-video-grid) [![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

## Install

```bash
// pick one
npm i --save https://gitlab.com/niu40/edsolabs-video-grid.git
yarn add https://gitlab.com/niu40/edsolabs-video-grid.git
```

## Usage

```jsx
import React, { Component } from 'react'

import {
  LiveGrid,         // grid realtime streaming layout
  LayoutingGrid,    // grid with zones editting layout
  FillingGrid,      // grid with sources editting layout
  Draggable,        // draging hoc
  ACTION_ADD_SOURCE // action constant for drag source into the grid
} from 'edsolabs-video-grid'

// pick one
import 'edsolabs-video-grid/dist/grid.scss' // node-sass version
import 'edsolabs-video-grid/dist/grid.css'  // css version

// checkout ./src/App.js for advanced usage
class Example extends Component {
  render() {
    return <LiveGrid
      columns={4}
      rows={4}
      sources={SOURCES_ARRAY_GOES_HERE}
      zones={ZONES_ARRAY_GOES_HERE}
    />
  }
}
```

## License

MIT © [gia@coa.vn](https://gitlab.com/niu40)
