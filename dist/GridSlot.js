import React, { useState } from 'react';
import { DATA_TRANSFER_ACTION, DATA_TRANSFER_PAYLOAD, ACTION_ADD_SOURCE, ACTION_MOVE_SOURCE, ACTION_MOVE_ZONE } from './Draggable';
export default function GridZone(props) {
  const [highlight, setHighlight] = useState(false);

  const addSource = source => {
    if (typeof props.onSourceAdd !== 'function') return;
    props.onSourceAdd(source);
  };

  const updateSource = (index, source) => {
    if (typeof props.onSourceUpdate !== 'function') return;
    props.onSourceUpdate(index, source);
  };

  const updateZone = (index, zone) => {
    if (typeof props.onZoneUpdate !== 'function') return;
    props.onZoneUpdate(index, zone);
  };

  const handleDragOver = e => {
    e.preventDefault();
    setHighlight(true);
  };

  const handleDragLeave = e => {
    e.preventDefault();
    setHighlight(false);
  };

  const handleDrop = e => {
    e.preventDefault();
    setHighlight(false);

    try {
      const action = e.dataTransfer.getData(DATA_TRANSFER_ACTION) || null;
      const payload = JSON.parse(e.dataTransfer.getData(DATA_TRANSFER_PAYLOAD) || null);
      if (!payload) return;
      console.log(action, payload);

      switch (action) {
        case ACTION_ADD_SOURCE:
          addSource({ ...payload,
            x: props.x,
            y: props.y,
            size: props.size
          });
          break;

        case ACTION_MOVE_SOURCE:
          updateSource(payload.index, { ...payload.source,
            x: props.x,
            y: props.y,
            size: props.size
          });
          break;

        case ACTION_MOVE_ZONE:
          updateZone(payload.index, { ...payload.zone,
            x: props.x,
            y: props.y
          });
          break;

        default:
      }
    } catch (e) {
      throw new Error('Invalid payload data');
    }
  };

  return /*#__PURE__*/React.createElement("section", {
    onDragOver: handleDragOver,
    onDragLeave: handleDragLeave,
    onDrop: handleDrop,
    className: `vw-zone ${props.demo ? 'vw-demo' : ''} ${highlight ? 'vw-highlight' : ''}`,
    style: {
      gridArea: `${props.y} / ${props.x} / span ${props.size} / span ${props.size}`
    }
  }, props.children);
}