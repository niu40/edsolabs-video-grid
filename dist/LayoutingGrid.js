function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React from 'react';
import Grid from './Grid';
import GridCell from './GridCell';
import GridSlot from './GridSlot';
import Draggable, { ACTION_MOVE_ZONE } from './Draggable';
const RESIZING_THRESHOLD = .25;
export default class LayoutingGrid extends React.Component {
  constructor(props) {
    super(props);

    _defineProperty(this, "state", {
      resizing: null
    });

    _defineProperty(this, "beginResize", (index, e) => {
      e.preventDefault();
      const zone = this.props.zones[index];
      if (!zone) return;
      this.setState({
        resizing: {
          index,
          screenX: e.screenX,
          screenY: e.screenY,
          deltaX: 0,
          deltaY: 0,
          x: zone.x,
          y: zone.y,
          size: zone.size
        }
      });
    });

    _defineProperty(this, "handleMouseMove", e => {
      const o = this.state.resizing;
      if (!o) return;
      this.setState({
        resizing: { ...o,
          deltaX: e.screenX - o.screenX,
          deltaY: e.screenY - o.screenY
        }
      });
    });

    _defineProperty(this, "handleMouseUp", e => {
      const o = this.state.resizing;
      if (!o) return;
      const zone = this.props.zones[o.index];
      if (!zone) return;
      const size = Math.ceil(this.resizingSize);
      this.updateZone(o.index, { ...zone,
        size
      });
      this.setState({
        resizing: null
      });
    });

    _defineProperty(this, "updateZone", (index, zone) => {
      const zones = [...this.props.zones];
      const item = { ...zones[index],
        ...zone
      };
      if (item.x + item.size > this.props.columns) item.size = Math.min(item.size, this.props.columns - item.x + 1, this.props.rows - item.y + 1);
      zones[index] = item;
      this.setZones(zones);
    });

    _defineProperty(this, "removeZone", index => {
      const zones = [...this.props.zones];
      zones.splice(index, 1);
      this.setZones(zones);
    });

    _defineProperty(this, "setZones", zones => {
      if (typeof this.props.onZonesChange !== 'function') return;
      this.props.onZonesChange(zones);
    });

    this.wrapperRef = /*#__PURE__*/React.createRef();
  }

  renderSlots() {
    const slots = [];

    for (let y = 1; y <= this.props.rows; y++) {
      for (let x = 1; x <= this.props.columns; x++) {
        slots.push( /*#__PURE__*/React.createElement(GridSlot, {
          key: `zone-${x}-${y}`,
          x: x,
          y: y,
          size: 1,
          onZoneUpdate: this.updateZone
        }));
      }
    }

    return slots;
  }

  renderCells() {
    return this.props.zones.map((i, k) => /*#__PURE__*/React.createElement(GridCell, _extends({}, i, {
      wireframe: true,
      renderer: this.props.zoneRenderer,
      key: `cell-${k}`
    }), /*#__PURE__*/React.createElement(Draggable, {
      action: ACTION_MOVE_ZONE,
      payload: {
        zone: i,
        index: k
      }
    }), /*#__PURE__*/React.createElement("span", {
      className: "vw-label"
    }, i.name || `ZONE ${k + 1}`), /*#__PURE__*/React.createElement("div", {
      className: "vw-actions"
    }, /*#__PURE__*/React.createElement("button", {
      onClick: this.removeZone.bind(this, k)
    }, "\xD7")), /*#__PURE__*/React.createElement("span", {
      className: "vw-resizer",
      onMouseDown: this.beginResize.bind(this, k)
    })));
  }

  get cellSize() {
    if (!this.wrapperRef.current) return [0, 0];
    const w = this.wrapperRef.current.offsetWidth / this.props.columns;
    const h = this.wrapperRef.current.offsetHeight / this.props.rows;
    return [w, h];
  }

  get resizingSize() {
    if (!this.state.resizing) return 1;
    if (!this.wrapperRef.current) return 1;
    const [w, h] = this.cellSize;
    const o = this.state.resizing;
    let delta = Math.max(o.deltaX / w, o.deltaY / h);
    if (delta + RESIZING_THRESHOLD > Math.ceil(delta)) delta = Math.ceil(delta);else if (delta - RESIZING_THRESHOLD < Math.floor(delta)) delta = Math.floor(delta);
    return o.size + delta;
  }

  renderResizingFrame() {
    if (!this.state.resizing) return null;
    const size = this.resizingSize;
    const o = this.state.resizing;
    const [w, h] = this.cellSize;
    const width = Math.min(Math.max(size, 1), this.props.columns) * w + 'px';
    const height = Math.min(Math.max(size, 1), this.props.rows) * h + 'px';
    return /*#__PURE__*/React.createElement("div", {
      className: "vw-resizing-frame",
      style: {
        top: (o.y - 1) * h + 'px',
        left: (o.x - 1) * w + 'px',
        width,
        height
      }
    });
  }

  render() {
    return /*#__PURE__*/React.createElement("div", {
      className: "vw-layouting",
      style: {
        cursor: this.state.resizing ? 'nw-resize' : 'auto'
      },
      onMouseMove: this.handleMouseMove,
      onMouseUp: this.handleMouseUp,
      ref: this.wrapperRef
    }, /*#__PURE__*/React.createElement(Grid, this.props, this.renderCells(), this.renderSlots(), this.renderResizingFrame()));
  }

}