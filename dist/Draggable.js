import React from 'react';
import PropTypes from 'prop-types';
export const DATA_TRANSFER_ACTION = 'action';
export const DATA_TRANSFER_PAYLOAD = 'payload';
export const ACTION_ADD_SOURCE = 'ADD_SOURCE';
export const ACTION_MOVE_SOURCE = 'MOVE_SOURCE';
export const ACTION_MOVE_ZONE = 'MOVE_ZONE';

function Draggable(props) {
  const handleDragStart = e => {
    e.dataTransfer.setData(DATA_TRANSFER_ACTION, props.action);
    e.dataTransfer.setData(DATA_TRANSFER_PAYLOAD, JSON.stringify(props.payload));
  };

  return /*#__PURE__*/React.createElement("div", {
    className: "vw-draggable",
    onDragStart: handleDragStart,
    draggable: !props.disabled
  }, props.children);
}

Draggable.propsTypes = {
  action: PropTypes.string,
  payload: PropTypes.any
};
Draggable.defaultProps = {
  action: null,
  payload: null
};
export default Draggable;