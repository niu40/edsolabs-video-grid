function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React from 'react';
import PropTypes from 'prop-types';
export default class GridCell extends React.Component {
  get className() {
    const res = ['vw-cell'];
    if (this.props.demo) res.push('vw-demo');
    if (this.props.wireframe) res.push('vw-wireframe');
    return res.join(' ');
  }

  render() {
    return /*#__PURE__*/React.createElement("div", {
      onDoubleClick: this.props.onFocus,
      className: this.className,
      style: {
        gridArea: `${this.props.y} / ${this.props.x} / span ${this.props.size} / span ${this.props.size}`
      }
    }, this.props.children);
  }

}

_defineProperty(GridCell, "propTypes", {
  x: PropTypes.number,
  y: PropTypes.number,
  size: PropTypes.number,
  demo: PropTypes.bool,
  name: PropTypes.string,
  wireframe: PropTypes.bool,
  onFocus: PropTypes.func
});

_defineProperty(GridCell, "defaultProps", {
  x: 1,
  y: 1,
  size: 1,
  demo: false,
  name: null,
  wireframe: false,
  onFocus: () => {}
});