function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React from 'react';
import Grid from './Grid';
import GridSlot from './GridSlot';
import GridCell from './GridCell';
import Draggable, { ACTION_MOVE_SOURCE } from './Draggable';
const RESIZING_THRESHOLD = .25;
export default class FillingGrid extends React.Component {
  constructor(props) {
    super(props);

    _defineProperty(this, "state", {
      resizing: null
    });

    _defineProperty(this, "renderSlots", () => {
      return [...this.props.zones, ...this.missingSlots].map((i, k) => /*#__PURE__*/React.createElement(GridSlot, _extends({}, i, {
        renderer: this.props.zoneRenderer,
        onSourceAdd: this.addSource,
        onSourceUpdate: this.updateSource,
        key: `zone-${k}`
      })));
    });

    _defineProperty(this, "renderCells", () => {
      return this.props.sources.map((i, k) => /*#__PURE__*/React.createElement(GridCell, _extends({}, i, {
        demo: true,
        renderer: this.props.cellRenderer,
        key: `cell-${k}`
      }), /*#__PURE__*/React.createElement(Draggable, {
        action: ACTION_MOVE_SOURCE,
        payload: {
          source: i,
          index: k
        }
      }), /*#__PURE__*/React.createElement("div", {
        className: "vw-actions"
      }, /*#__PURE__*/React.createElement("button", null, i.name || `SOURCE ${k + 1}`), /*#__PURE__*/React.createElement("button", {
        onClick: this.removeSource.bind(this, k)
      }, "\xD7")), /*#__PURE__*/React.createElement("span", {
        className: "vw-resizer",
        onMouseDown: this.beginResize.bind(this, k)
      })));
    });

    _defineProperty(this, "beginResize", (index, e) => {
      e.preventDefault();
      const source = this.props.sources[index];
      if (!source) return;
      this.setState({
        resizing: {
          index,
          screenX: e.screenX,
          screenY: e.screenY,
          deltaX: 0,
          deltaY: 0,
          x: source.x,
          y: source.y,
          size: source.size
        }
      });
    });

    _defineProperty(this, "handleMouseMove", e => {
      const o = this.state.resizing;
      if (!o) return;
      this.setState({
        resizing: { ...this.state.resizing,
          deltaX: e.screenX - o.screenX,
          deltaY: e.screenY - o.screenY
        }
      });
    });

    _defineProperty(this, "handleMouseUp", e => {
      const o = this.state.resizing;
      if (!o) return;
      const sources = this.props.sources[o.index];
      if (!sources) return;
      const size = Math.ceil(this.resizingSize);
      this.updateSource(o.index, { ...sources,
        size
      });
      this.setState({
        resizing: null
      });
    });

    _defineProperty(this, "addSource", source => {
      this.setSources([...this.props.sources, source]);
    });

    _defineProperty(this, "updateSource", (index, source) => {
      const sources = [...this.props.sources];
      sources[index] = { ...sources[index],
        ...source
      };
      this.setSources(sources);
    });

    _defineProperty(this, "removeSource", index => {
      const sources = [...this.props.sources];
      sources.splice(index, 1);
      this.setSources(sources);
    });

    _defineProperty(this, "setSources", sources => {
      if (typeof this.props.onSourcesChange !== 'function') return;
      this.props.onSourcesChange(sources);
    });

    this.wrapperRef = /*#__PURE__*/React.createRef();
  }

  get missingSlots() {
    const zones = [];

    for (let x = 1; x <= this.props.columns; x++) {
      for (let y = 1; y <= this.props.rows; y++) {
        let overlap = false;

        for (let i = 0; i < this.props.zones.length; i++) {
          const zone = this.props.zones[i];

          if (x >= zone.x && x < zone.x + zone.size && y >= zone.y && y < zone.y + zone.size) {
            overlap = true;
            break;
          }
        }

        if (!overlap) zones.push({
          x,
          y,
          size: 1
        });
      }
    }

    return zones;
  }

  get cellSize() {
    if (!this.wrapperRef.current) return [0, 0];
    const w = this.wrapperRef.current.offsetWidth / this.props.columns;
    const h = this.wrapperRef.current.offsetHeight / this.props.rows;
    return [w, h];
  }

  get resizingSize() {
    if (!this.state.resizing) return 1;
    if (!this.wrapperRef.current) return 1;
    const [w, h] = this.cellSize;
    const o = this.state.resizing;
    let delta = Math.max(o.deltaX / w, o.deltaY / h);
    if (delta + RESIZING_THRESHOLD > Math.ceil(delta)) delta = Math.ceil(delta);else if (delta - RESIZING_THRESHOLD < Math.floor(delta)) delta = Math.floor(delta);
    return o.size + delta;
  }

  renderResizingFrame() {
    if (!this.state.resizing) return null;
    const size = this.resizingSize;
    const o = this.state.resizing;
    const [w, h] = this.cellSize;
    const width = Math.min(Math.max(size, 1), this.props.columns) * w + 'px';
    const height = Math.min(Math.max(size, 1), this.props.rows) * h + 'px';
    return /*#__PURE__*/React.createElement("div", {
      className: "vw-resizing-frame",
      style: {
        top: (o.y - 1) * h + 'px',
        left: (o.x - 1) * w + 'px',
        width,
        height
      }
    });
  }

  render() {
    return /*#__PURE__*/React.createElement("div", {
      className: "vw-filling",
      style: {
        cursor: this.state.resizing ? 'nw-resize' : 'auto'
      },
      onMouseMove: this.handleMouseMove,
      onMouseUp: this.handleMouseUp,
      ref: this.wrapperRef
    }, /*#__PURE__*/React.createElement(Grid, this.props, this.renderSlots(), this.renderCells(), this.renderResizingFrame()));
  }

}