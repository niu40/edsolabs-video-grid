function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React from 'react';
import Grid from './Grid';
import GridCell from './GridCell';
export default class LiveGrid extends React.Component {
  constructor(...args) {
    super(...args);

    _defineProperty(this, "state", {
      focusing: null
    });

    _defineProperty(this, "focus", (e, item, index) => {
      e.preventDefault();
      this.setState({
        focusing: index
      });
    });

    _defineProperty(this, "blur", e => {
      e.preventDefault();
      this.setState({
        focusing: null
      });
    });

    _defineProperty(this, "renderCells", () => {
      // on focus
      if (this.state.focusing !== null) {
        const focusing = this.props.sources[this.state.focusing];

        if (focusing) {
          return /*#__PURE__*/React.createElement(GridCell, _extends({}, focusing, {
            key: `cell-${this.state.focusing}`,
            onFocus: this.blur
          }), typeof this.props.cellRenderer === 'function' && this.props.cellRenderer(focusing, this.state.focusing));
        }
      } // off focus


      return this.props.sources.map((i, k) => /*#__PURE__*/React.createElement(GridCell, _extends({}, i, {
        key: `cell-${k}`,
        onFocus: e => this.focus(e, i, k)
      }), typeof this.props.cellRenderer === 'function' && this.props.cellRenderer(i, k)));
    });
  }

  render() {
    return /*#__PURE__*/React.createElement("div", {
      className: "vw-live"
    }, /*#__PURE__*/React.createElement(Grid, _extends({}, this.props, {
      isFocusing: this.state.focusing !== null
    }), this.renderCells()));
  }

}