import React from 'react'
import LiveGrid from './components/LiveGrid'
import LayoutingGrid from './components/LayoutingGrid'
import FillingGrid from './components/FillingGrid'
import Draggable, { ACTION_ADD_SOURCE } from './components/Draggable'
import './app.scss'

const modes = ['default', 'layouting', 'filling']

export default class App extends React.Component {
  state = {
    mode: modes[1],
    zones: [
      { x: 1, y: 1, size: 2 },
      { x: 3, y: 2, size: 2 },
      { x: 3, y: 1, size: 1 },
      { x: 4, y: 1, size: 1 },
      { x: 1, y: 4, size: 1 },
      { x: 2, y: 4, size: 1 }
    ],
    sources: [
      { x: 1, y: 1, size: 3, demo: true },
      { x: 4, y: 1, size: 1, demo: true },
      { x: 4, y: 4, size: 1, demo: true }
    ]
  }
  gridRef = React.createRef()

  handleModeChange = (e) => {
    this.setState({ mode: e.target.value })
  }

  render() {
    return (

      <div className="vw-app">
        {
          modes.map(i => (
            <label key={i}>
              <input
                type="radio"
                name="mode"
                checked={this.state.mode === i}
                value={i}
                onChange={this.handleModeChange} />{i}
            </label>
          ))
        }
        <section>
          <button onClick={() => this.setState({ sources: [] })}>Remove all sources</button>
          <button onClick={() => this.setState({ zones: [] })}>Remove all zones</button>
          <button onClick={() => this.setState({ zones: [...this.state.zones, { x: 1, y: 1, size: 1 }] })}>Add zone</button>
        </section>

        {
          this.state.mode === 'default' &&
          <div>
            <LiveGrid
              ref={this.gridRef}
              rows={4}
              columns={4}
              zones={this.state.zones}
              sources={this.state.sources}
              cellRenderer={(i, k) => <span>custom cell renderer</span>} />
          </div>
        }

        {
          this.state.mode === 'layouting' &&
          <div>
            <LayoutingGrid
              ref={this.gridRef}
              rows={4}
              columns={4}
              zones={this.state.zones}
              onZonesChange={zones => this.setState({ zones })} />
          </div>
        }

        {
          this.state.mode === 'filling' &&
          <div>
            <Draggable action={ACTION_ADD_SOURCE} payload={{name: 'cam1', demo: true}}><button>cam1</button></Draggable>
            <Draggable action={ACTION_ADD_SOURCE} payload={{name: 'cam2', demo: true}}><button>cam2</button></Draggable>
            <Draggable action={ACTION_ADD_SOURCE} payload={{name: 'cam3', demo: true}}><button>cam3</button></Draggable>
            <FillingGrid
              ref={this.gridRef}
              rows={4}
              columns={4}
              zones={this.state.zones}
              sources={this.state.sources}
              onSourcesChange={sources => this.setState({ sources })} />
          </div>
        }

        <pre>
          <code>
            {
              JSON.stringify({
                mode: this.state.mode,
                sources: this.state.sources,
                zones: this.state.zones
              }, null, 2)
            }
          </code>
        </pre>
      </div>
    )
  }
}
