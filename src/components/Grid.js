import React from 'react'
import PropTypes from 'prop-types'

export default class Grid extends React.Component {
  static propTypes = {
    rows: PropTypes.number,
    columns: PropTypes.number,
    gap: PropTypes.number,
    showDividers: PropTypes.bool,
    isFocusing: PropTypes.bool,
    sources: PropTypes.arrayOf(PropTypes.shape({
      x: PropTypes.number,
      y: PropTypes.number,
      size: PropTypes.number,
      demo: PropTypes.bool
    })),
    zones: PropTypes.arrayOf(PropTypes.shape({
      x: PropTypes.number,
      y: PropTypes.number,
      size: PropTypes.number
    }))
  }

  static defaultProps = {
    rows: 4,
    columns: 4,
    gap: 1,
    sources: [],
    zones: [],
    showDividers: true,
    isFocusing: false
  }

  state = {
    zones: [],
    sources: []
  }

  renderDividers = () => {
    if (!this.props.gap) return null
    return (
      <div className="vw-dividers">
        {[...Array(this.props.rows - 1).keys()].map(i => <div
          key={`row-${i}`}
          className="vw-by-row"
          style={{
            top: `${(i + 1) * 100 / this.props.rows}%`,
            height: `${this.props.gap}px`,
            marginTop: `${this.props.gap / -2}px`
          }} />
        )}
        {[...Array(this.props.columns - 1).keys()].map(i => <div
          key={`column-${i}`}
          className="vw-by-column"
          style={{
            left: `${(i + 1)* 100 / this.props.columns}%`,
            width: `${this.props.gap}px`,
            marginLeft: `${this.props.gap / -2}px`
          }} />
        )}
      </div>
    )
  }

  render() {
    return (
      <div className="vw-wrapper">
        {this.props.showDividers && this.renderDividers()}
        <div className="vw-grid" style={{
          display: this.props.isFocusing ? 'flex' : undefined,
          gridTemplateRows: `repeat(${this.props.rows}, 1fr)`,
          gridTemplateColumns: `repeat(${this.props.columns}, 1fr)`,
          gridGap: `${this.props.gap}px`
        }}>
          {this.props.children}
        </div>
      </div>
    )
  }
}