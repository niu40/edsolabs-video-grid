import React from 'react'
import PropTypes from 'prop-types'

export default class GridCell extends React.Component {
  static propTypes = {
    x: PropTypes.number,
    y: PropTypes.number,
    size: PropTypes.number,
    demo: PropTypes.bool,
    name: PropTypes.string,
    wireframe: PropTypes.bool,
    onFocus: PropTypes.func
  }

  static defaultProps = {
    x: 1,
    y: 1,
    size: 1,
    demo: false,
    name: null,
    wireframe: false,
    onFocus: () => {}
  }

  get className() {
    const res = ['vw-cell']
    if (this.props.demo) res.push('vw-demo')
    if (this.props.wireframe) res.push('vw-wireframe')
    return res.join(' ')
  }

  render() {
    return (
      <div
        onDoubleClick={this.props.onFocus}
        className={this.className}
        style={{
          gridArea: `${this.props.y} / ${this.props.x} / span ${this.props.size} / span ${this.props.size}`
        }}>
        {this.props.children}
      </div>
    )
  }
}