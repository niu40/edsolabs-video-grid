import React from 'react'
import Grid from './Grid'
import GridCell from './GridCell'
import GridSlot from './GridSlot'
import Draggable, { ACTION_MOVE_ZONE } from './Draggable'

const RESIZING_THRESHOLD = .25

export default class LayoutingGrid extends React.Component {
  state = {
    resizing: null
  }

  constructor(props) {
    super(props)
    this.wrapperRef = React.createRef()
  }

  renderSlots() {
    const slots = []
    for (let y = 1; y <= this.props.rows; y++) {
      for (let x = 1; x <= this.props.columns; x++) {
        slots.push(
          <GridSlot
            key={`zone-${x}-${y}`}
            x={x} y={y} size={1}
            onZoneUpdate={this.updateZone} />
        )
      }
    }
    return slots
  }

  renderCells() {
    return this.props.zones.map((i, k) => (
      <GridCell
        {...i}
        wireframe={true}
        renderer={this.props.zoneRenderer}
        key={`cell-${k}`}>
        <Draggable
          action={ACTION_MOVE_ZONE}
          payload={{ zone: i, index: k }} />
        <span className="vw-label">{i.name || `ZONE ${k + 1}`}</span>
        <div className="vw-actions">
          <button onClick={this.removeZone.bind(this, k)}>&times;</button>
        </div>
        <span className="vw-resizer" onMouseDown={this.beginResize.bind(this, k)}></span>
      </GridCell>
    ))
  }

  get cellSize() {
    if (!this.wrapperRef.current) return [0, 0]
    const w = this.wrapperRef.current.offsetWidth / this.props.columns
    const h = this.wrapperRef.current.offsetHeight / this.props.rows
    return [w, h]
  }

  get resizingSize() {
    if (!this.state.resizing) return 1
    if (!this.wrapperRef.current) return 1
    const [w, h] = this.cellSize
    const o = this.state.resizing
    let delta = Math.max(o.deltaX / w, o.deltaY / h)
    if (delta + RESIZING_THRESHOLD > Math.ceil(delta)) delta = Math.ceil(delta)
    else if (delta - RESIZING_THRESHOLD < Math.floor(delta)) delta = Math.floor(delta)
    return o.size + delta
  }

  renderResizingFrame() {
    if (!this.state.resizing) return null
    const size = this.resizingSize
    const o = this.state.resizing
    const [w, h] = this.cellSize
    const width = Math.min(Math.max(size, 1), this.props.columns) * w + 'px'
    const height = Math.min(Math.max(size, 1), this.props.rows) * h + 'px'
    return <div
      className="vw-resizing-frame"
      style={{
        top: (o.y - 1) * h + 'px',
        left: (o.x - 1) * w + 'px',
        width,
        height
      }}
    />
  }

  render() {
    return (
      <div
        className="vw-layouting"
        style={{ cursor: this.state.resizing ? 'nw-resize' : 'auto' }}
        onMouseMove={this.handleMouseMove}
        onMouseUp={this.handleMouseUp}
        ref={this.wrapperRef}>
        <Grid {...this.props}>
          {this.renderCells()}
          {this.renderSlots()}
          {this.renderResizingFrame()}
        </Grid>
      </div>
    )
  }

  beginResize = (index, e) => {
    e.preventDefault()
    const zone = this.props.zones[index]
    if (!zone) return
    this.setState({
      resizing: {
        index,
        screenX: e.screenX,
        screenY: e.screenY,
        deltaX: 0,
        deltaY: 0,
        x: zone.x,
        y: zone.y,
        size: zone.size
      }
    })
  }

  handleMouseMove = e => {
    const o = this.state.resizing
    if (!o) return
    this.setState({
      resizing: {
        ...o,
        deltaX: e.screenX - o.screenX,
        deltaY: e.screenY - o.screenY
      }
    })
  }

  handleMouseUp = e => {
    const o = this.state.resizing
    if (!o) return
    const zone = this.props.zones[o.index]
    if (!zone) return
    const size = Math.ceil(this.resizingSize)
    this.updateZone(o.index, {
      ...zone,
      size
    })
    this.setState({
      resizing: null
    })
  }

  updateZone = (index, zone) => {
    const zones = [...this.props.zones]
    const item = { ...zones[index], ...zone}
    if (item.x + item.size > this.props.columns)
    item.size = Math.min(item.size, this.props.columns - item.x + 1, this.props.rows - item.y + 1)
    zones[index] = item
    this.setZones(zones)
  }

  removeZone = index => {
    const zones = [...this.props.zones]
    zones.splice(index, 1)
    this.setZones(zones)
  }

  setZones = zones => {
    if (typeof this.props.onZonesChange !== 'function') return
    this.props.onZonesChange(zones)
  }
}
