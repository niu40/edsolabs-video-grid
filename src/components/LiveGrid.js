import React from 'react'
import Grid from './Grid'
import GridCell from './GridCell'

export default class LiveGrid extends React.Component {
  state = {
    focusing: null
  }

  focus = (e, item, index) => {
    e.preventDefault()
    this.setState({
      focusing: index
    })
  }

  blur = e => {
    e.preventDefault()
    this.setState({
      focusing: null
    })
  }
  
  renderCells = () => {
    // on focus
    if (this.state.focusing !== null) {
      const focusing = this.props.sources[this.state.focusing]
      if (focusing) {
        return <GridCell {...focusing} key={`cell-${this.state.focusing}`} onFocus={this.blur}>
          {
            typeof this.props.cellRenderer === 'function'
            && this.props.cellRenderer(focusing, this.state.focusing)
          }
        </GridCell>
      }
    }
    // off focus
    return this.props.sources.map((i, k) => (
      <GridCell
        {...i}
        key={`cell-${k}`}
        onFocus={e => this.focus(e, i, k)}>
        {
          typeof this.props.cellRenderer === 'function'
          && this.props.cellRenderer(i, k)
        }
      </GridCell>
    ))
  }

  render() {
    return (
      <div className="vw-live">
        <Grid {...this.props} isFocusing={this.state.focusing !== null}>
          {this.renderCells()}
        </Grid>
      </div>
    )
  }
}